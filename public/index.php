<?php

    // configuration
    require("../includes/config.php");
    $categories = query("SELECT * FROM categories");

    render("categories.php", ["title" => "Categories", "categories" => $categories]);

?>
